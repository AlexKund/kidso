import axios from "axios";

export const fetchPages = async () => {
  try {
    let response = await axios({
      method: "GET",
      url: '../content_1.json',
    });
    if (response.status === 200) {
      return response.data
    }
  } catch (e) {
    console.log("Error occurred")
  }
};