const NavLink = props => <a href="/#" onClick={props.handleNavCLick}>{props.children}</a>;

export default NavLink;