import React, {useEffect, useState} from "react";

import Home from './pages/Home';
import NotFound from './pages/NotFound';
import Page from './pages/Page';
import NavLink from './NavLink';

import {fetchPages} from './helpers';

import {
  Switch,
  Route,
  Link,
  useHistory,
  useLocation
} from "react-router-dom";

export default function Routes() {
  const [pagesContent, setPagesContent] = useState([]);
  const [pageContent, setPageContent] = useState({});
  const history = useHistory();
  const location = useLocation();
  const metaDescription = document.querySelector("meta[name='description']");

  useEffect(() => {
    fetchPages().then(res => {
      const pages = res.page;
      setPagesContent(pages);

      metaDescription.setAttribute('content', '');
      document.title = '';

      pages.map(e => {
        if(e[location.pathname]) {
          setPageContent(e[location.pathname]);
          metaDescription.setAttribute('content', e[location.pathname].meta?.meta_description);
          document.title = e[location.pathname].meta?.meta_title;
        }

        return e;
      });
    });
  }, [location, metaDescription]);

  const handleNavCLick = (e, url) => {
    e.preventDefault();
    pagesContent.map(el => {
      if (el[url]) {
        history.push(url);
        return setPageContent(el[url]);
      }
      return el;
    });
  };

  return (
    <div>
      <ul>
        <li>
          <Link to="/">Home</Link>
        </li>
        <li>
          <NavLink handleNavCLick={e => handleNavCLick(e, "/content/zamyana-na-stoka.html")}>Goods replacement</NavLink>
        </li>
        <li>
          <NavLink handleNavCLick={e => handleNavCLick(e, "/content/chesto-zadavani-vaprosi.html")}>FAQ</NavLink>
        </li>
      </ul>

      <hr/>

      <Switch>
        <Route exact path="/">
          <Home/>
        </Route>
        <Route path="/:content(content)/zamyana-na-stoka.html">
          <Page page={pageContent}/>
        </Route>

        <Route path="/:content(content)/chesto-zadavani-vaprosi.html">
          <Page page={pageContent}/>
        </Route>
        <Route>
          <NotFound/>
        </Route>
      </Switch>
    </div>
  );
}
