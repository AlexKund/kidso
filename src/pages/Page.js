const Page = props => (
    <div>
      <h2>{props.page.title}</h2>
      <div dangerouslySetInnerHTML={{__html: props.page.content}}/>
    </div>
  );

export default Page;